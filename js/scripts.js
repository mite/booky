$(document).ready(function(){
    $("#datepicker" ).datepicker();
    
    $("#filtered_search").click(function(even){
        event.preventDefault();
        console.log("Button Clicked");
        var isbn = $("#isbn").val();
        $("#title").val = "";
        $("#authors").val = "";
        $("#publisher").val = "";
        $("#year").val = "";
        $("#search_results").html("");
        console.log(isbn);
        var finnaAPI = "https://api.finna.fi/api/v1/search?";
        $.getJSON(finnaAPI,{
           lookfor: isbn
       })
       .done(function(result){
            var record_count = result.resultCount;
            if(record_count > 0)
                $("#record_found").text("Records found " + result.resultCount + "!");
            else
                $("#record_found").text("Record not found!");
            var counter = 0;
            $("#title").val(result.records[0].title);
                $("#authors").val(result.records[0].nonPresenterAuthors[0].name);
                
                var avatar_url = 'https://api.finna.fi/Cover/Show?isbn=' + isbn + '&limit=1';
                var avatar= '<div class=' + '\"' + 'avatar col-lg-push-3 col-md-push-4 col-sm-6' + '\"' + '>' +
                                        '<div class=' + '\"' + 'thumbnail' + '\"' + '>' +
                                            '<a href=' + '\"' + '#' + '\"' + '>' +
                                                '<img src=' + '\"' + avatar_url + '\"' + 'alt=' + '\"' + '' + '\"' + '\>' +
                                                '<div class=' + '\"' + 'caption' + '\"' + '>' +
                                                    '<h4><b>' + result.records[0].title + '</b></h4>' +
                                                    '<p>' + result.records[0].nonPresenterAuthors[0].name + '</p>' +
                                                '</div>' +
                                            '</a>' +
                                      '</div>' +
                            '</div>';
               $("#search_results").append(avatar);
        })
        .fail(function() {
            console.log( "error" );
        })
        .always(function() {
            console.log( "complete" );
        });
    });
    
    
    /****
    
    Getting some images from flickr
    ****/
    
    var flickerAPI = "http://api.flickr.com/services/feeds/photos_public.gne?jsoncallback=?";
       $.getJSON( flickerAPI, {
           tags: "ebook",
           tagmode: "any",
           format: "json"
       })
       .done(function( data ) {
           var count=0;
           $.each( data.items, function( i, item ) {
               count++;
               var found_image = '<div class=' + '\"' + 'col-lg-3 col-md-4 col-sm-6' + '\"' + '>' +
                                        '<div class=' + '\"' + 'thumbnail' + '\"' + '>' +
                                            '<a href=' + '\"' + '#' + '\"' + '>' +
                                                '<img src=' + '\"' + item.media.m + '\"' + 'alt=' + '\"' + '' + '\"' + '\>' +
                                                '<div class=' + '\"' + 'caption' + '\"' + '>' +
                                                    '<h4><b>' + item.title + '</b></h4>' +
                                                    '<p>' + item.by + '</p>' +
                                                '</div>' +
                                            '</a>' +
                                      '</div>' +
                                '</div>';
               $("#search_results").append(found_image);
               if ( i == 9 ) return false;
      });
           console.log(count + " records found!")
    });
});